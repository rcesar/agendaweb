#-*-coding:utf-8-*-
#qpy:webapp:Agenda
#qpy:fullscreen
#qpy://127.0.0.1:8080
#
from Bottle import Bottle,static_file
from Model import Model
from compromissos import Compromissos
from pendencias import Pendencias
from cadastro import Cadastro
from notas import Notas
import os,sys

#Coletanea de funcoes uteis
class helpers:
    def data2db(self,data):
        dia,mes,ano=data.split('/')
        return(ano+'-'+mes+'-'+dia)
    def db2data(self,data):
        ano,mes,dia=data.split("-")
        return(dia+"/"+mes+"/"+ano)

class Agenda(Bottle):
    def __init__(self):
        Bottle.__init__(self)
        self.helpers=helpers()
        self.appdir=os.path.dirname(os.path.abspath(sys.argv[0]))
        self.static_dir=os.path.join(self.appdir,'static')
        self.db=Model(self)
        #Rotas basicas da aplicação, as demais são definidas nos próprios modulos
        self.route('/static/<filename:path>',['GET','POST'],self.static)
        self.route('/css/<filename:path>',['GET','POST'],self.css)
        self.route('/js/<filename:path>',['GET','POST'],self.js)
        self.route('/#index.*#',['GET','POST'],lambda:self.static('index.html'))
        self.route('/',['GET','POST'],lambda:self.static('index.html'))
        #
        Compromissos(self)
        Pendencias(self)
        Notas(self)
        Cadastro(self)

    def static(self,filename):return(static_file(filename,self.static_dir))
    def css(self,filename):return(static_file(filename,os.path.join(self.static_dir,'css')))
    def js(self,filename):return(static_file(filename,os.path.join(self.static_dir,'js')))


if __name__=="__main__":
    app=Agenda()
    app.run(host='127.0.0.1',port=8080,reloader=True)
