#
#-*-coding:utf-8-*-

from Bottle import request,route,template
import json

POST='POST'
GET='GET'

class enderecos:
    def __init__(self,parent):
        self.db=parent.db
        self.servidor=parent.servidor
        self.encode=parent.encode

    def abrir(self,rowid):
        campo=self.db.execute("SELECT rowid,descricao,rua,bairro,cidade,estado,cep FROM enderecos WHERE rowid=?",(rowid)).fetchone()
        if len(campo)<=0:
            abort(404)
            return
        return(self.encode({'rowid':campo[0],'descricao':campo[1],'rua':campo[2],'bairro':campo[3],'cidade':campo[4],'estado':campo[5],'cep':campo[6]}))

    def listar(self,codigo):
        tpl = '''
             <li><table>
               <tr><td colspan=2>
                <a href='javascript:enderecos.abrir("{{rowid}}");'>{{descricao}}</a>
                <img src="/static/imagens/lixeira.png" onClick="enderecos.apagar('{{rowid}}');">
               </td></tr>
               <tr><td colspan=2>{{rua}}</td></tr>
               <tr><td>{{bairro}}</td><td>{{cidade}}</td></tr>
               <tr><td>{{estado}}</td><td>{{cep}}</td></tr></table>
            '''
        saida="<ul>"
        lista=self.db.execute("SELECT rowid,descricao,rua,bairro,cidade,estado,cep FROM enderecos WHERE codigo=?",(codigo,)).fetchall()
        for campo in lista:
            saida+=template(tpl,{'rowid':campo[0],'descricao':campo[1],'rua':campo[2],'bairro':campo[3],'cidade':campo[4],'estado':campo[5],'cep':campo[6]})
        saida+="</ul><br><button onClick='enderecos.novo();'><img src='/static/imagens/novo.png'>Novo</button>"
        return(saida)

    def apagar(self,rowid):
        try:
            self.db.execute("DELETE FROM enderecos WHERE rowid=?",(rowid))
            self.db.commit()
            return("Registro apagado")
        except Exception as e:
            return(str(e))

    def salvar(self,codigo):
        descricao=request.forms.get('descricao')
        rua=request.forms.get('rua')
        bairro=request.forms.get('bairro')
        cidade=request.forms.get('cidade')
        estado=request.forms.get('estado')
        cep=request.forms.get('cep')
        rowid=request.forms.get("rowid")
        if not(descricao) or not(rua) or not(bairro) or not(cidade) or not(estado) or not(cep):return("Erro campo obrigatorio não informado")
        contador=self.db.execute('SELECT count(descricao) FROM enderecos WHERE codigo=? AND descricao=?',(codigo,descricao)).fetchone()[0]
        if contador<=0 and (not(rowid) or rowid.lower()=='novo'):
            self.db.execute("INSERT INTO enderecos(codigo,descricao,rua,bairro,cidade,estado,cep) VALUES(?,?,?,?,?,?,?)",
                            (codigo,descricao,rua,bairro,cidade,estado,cep))
            self.db.commit()
            return("Registro inserido")
        else:
            self.db.execute("""UPDATE enderecos SET descricao=?,rua=?,bairro=?,cidade=?,estado=?,cep=?
             WHERE rowid=?""",(descricao,rua,bairro,cidade,estado,cep,rowid))
            self.db.commit()
            return("Registro alterado ")
#---------------------------------------------------------------    
class telefone:
    def __init__(self,parent):
        self.db=parent.db
        self.servidor=parent.servidor
        self.encode=parent.encode

    def abrir(self,rowid):
        campo=self.db.execute("SELECT rowid,descricao,valor FROM telefones WHERE rowid=?",(rowid,)).fetchone()
        if len(campo)<=0:
            abort(404)
            return
        return(self.encode({'rowid':campo[0],'descricao':campo[1],'valor':campo[2]}))

    def salvar(self,codigo):
        rowid=request.forms.get("rowid")
        descricao=request.forms.get('descricao')
        valor=request.forms.get('valor')
        if not(descricao) or not(valor):return("Erro campo obrigatorio não informado")
        contador=self.db.execute('SELECT count(descricao) FROM telefones WHERE codigo=? AND descricao=?',(codigo,descricao)).fetchone()[0]
        if contador<=0 and (not(rowid) or rowid.lower()=='novo'):
            self.db.execute("INSERT INTO telefones(codigo,descricao,valor) VALUES(?,?,?)",(codigo,descricao,valor))
            self.db.commit()
            return("Registro inserido")
        else:
            self.db.execute("""UPDATE telefones SET descricao=?,valor=? WHERE rowid=?""",(descricao,valor,rowid))
            self.db.commit()
            return("Registro alterado ")

    def apagar(self,rowid):
        try:
            self.db.execute("DELETE FROM telefones WHERE rowid=?",(rowid))
            self.db.commit()
            return("Registro apagado")
        except Exception as e:
            return(str(e))

    def listar(self,codigo):
        tpl=''' <tr><th>
                <a href="javascript:telefones.abrir('{{rowid}}');">{{descricao}}</th></tr><tr><td>{{valor}}
                <img src='/static/imagens/lixeira.png' onClick='telefones.apagar("{{rowid}}");'>
            </td></tr>  '''
        saida="<table>"
        for campo in self.db.execute("SELECT rowid,descricao,valor FROM telefones WHERE codigo=?",(codigo,)).fetchall():
            saida+=template(tpl,{'rowid':campo[0],'descricao':campo[1],'valor':campo[2]})
        saida+="</table>"+"<br><button onClick='telefones.novo();'><img src='/static/imagens/novo.png'>Novo</button>"
        return(saida)
#---------
class internet:
    def __init__(self,parent):
        self.db=parent.db
        self.servidor=parent.servidor
        self.encode=parent.encode

    def salvar(self,codigo):
        rowid=request.forms.get("rowid")
        descricao=request.forms.get('descricao')
        valor=request.forms.get('valor')
        if not(descricao) or not(valor):return("Erro campo obrigatorio não informado")
        contador=self.db.execute('SELECT count(descricao) FROM internet WHERE codigo=? AND descricao=?',(codigo,descricao)).fetchone()[0]
        if contador<=0 and (not(rowid) or rowid.lower()=='novo'):
            self.db.execute("INSERT INTO internet(codigo,descricao,valor) VALUES(?,?,?)",(codigo,descricao,valor))
            self.db.commit()
            return("Registro inserido")
        else:
            self.db.execute("""UPDATE internet SET descricao=?,valor=? WHERE rowid=?""",(descricao,valor,rowid))
            self.db.commit()
            return("Registro alterado ")

    def abrir(self,rowid):
        campo=self.db.execute("SELECT rowid,descricao,valor FROM internet WHERE rowid=?",(rowid)).fetchone()
        if len(campo)<=0:
            abort(404)
            return
        return(self.encode({'rowid':campo[0],'descricao':campo[1],'valor':campo[2]}))

    def apagar(self,rowid):
        try:
            self.db.execute("DELETE FROM internet WHERE rowid=?",(rowid))
            self.db.commit()
            return("Registro apagado")
        except Exception as e:
            return(str(e))

    def listar(self,codigo):
        tpl=''' <tr><th>
                <a href="javascript:internet.abrir('{{rowid}}');">{{descricao}}</th></tr><tr><td>{{valor}}
                <img src='/static/imagens/lixeira.png' onClick='internet.apagar("{{rowid}}");'>
            </td></tr>  '''
        saida="<table>"
        for campo in self.db.execute("SELECT rowid,descricao,valor FROM internet WHERE codigo=?",(codigo,)).fetchall():
            saida+=template(tpl,{'rowid':campo[0],'descricao':campo[1],'valor':campo[2]})
        saida+="</table>"+"<br><button onClick='internet.novo();'><img src='/static/imagens/novo.png'>Novo</button>"
        return(saida)
#---------------------------------------------------------------    
class Cadastro:
    def __init__(self,servidor):
        self.db=servidor.db
        self.servidor=servidor
        self.encode=lambda ret:json.encoder.JSONEncoder().encode(ret)
        self.enderecos=enderecos(self)
        self.internet=internet(self)
        self.telefone=telefone(self)
        servidor.route('/cadastro/buscar',[POST],self.busca)
        servidor.route('/cadastro/salvar/<codigo>',[POST,],self.salvar)
        servidor.route('/cadastro/listar/<tipo>/<codigo>',[POST,GET],self.listar)
        servidor.route('/cadastro/abrir/<codigo>',[POST,GET],self.abrir)
        servidor.route('/cadastro/apagar/<codigo>',[POST,GET],self.apagar)
        #----------------
        servidor.route('/cadastro/endereco/abrir/<rowid>',[POST,GET],self.enderecos.abrir)
        servidor.route('/cadastro/endereco/salvar/<codigo>',[POST],self.enderecos.salvar)
        servidor.route('/cadastro/endereco/apagar/<rowid>',[POST,GET],self.enderecos.apagar)
        #-----
        servidor.route('/cadastro/telefone/abrir/<rowid>',[POST,GET],self.telefone.abrir)
        servidor.route('/cadastro/telefone/salvar/<codigo>',[POST],self.telefone.salvar)
        servidor.route('/cadastro/telefone/apagar/<rowid>',[POST,GET],self.telefone.apagar)
        #-----
        servidor.route('/cadastro/internet/abrir/<rowid>',[POST,GET],self.internet.abrir)
        servidor.route('/cadastro/internet/salvar/<codigo>',[POST],self.internet.salvar)
        servidor.route('/cadastro/internet/apagar/<rowid>',[POST,GET],self.internet.apagar)

    def apagar(self,codigo):
        try:
            self.db.execute("DELETE FROM agenda WHERE codigo=?",(codigo,))
            self.db.execute("DELETE FROM enderecos WHERE codigo=?",(codigo,))
            self.db.execute("DELETE FROM telefones WHERE codigo=?",(codigo,))
            self.db.execute("DELETE FROM internet WHERE codigo=?",(codigo,))
            self.db.commit()
            return("Registro apagado")
        except Exception as e:
            return(e)

    def abrir(self,codigo):
        nome=self.db.execute('SELECT nome FROM agenda WHERE codigo=?',(codigo,)).fetchone()[0]
        return(nome)

    def busca(self):
        tpl = '''<br> <a href='javascript:abrir(\"{{codigo}}\");'>{{nome}}</a><img src="/static/imagens/lixeira.png" onClick="apagar('{{codigo}}');">'''
        saida=''
        pad = request.forms.get('pad')
        lista = self.db.execute("SELECT codigo,nome FROM agenda where nome like ?",(pad,)).fetchall()
        for item in lista:
            saida+=template(tpl,{'codigo':item[0],'nome':item[1]})
        return(saida)
                
    def listar(self,tipo,codigo):
        if tipo=='enderecos':
            return(self.enderecos.listar(codigo))
        if tipo=='internet':
            return(self.internet.listar(codigo))
        if tipo=='telefones':
            return(self.telefone.listar(codigo))
        abort(404)

    def salvar(self,codigo='novo'):
        nome=request.forms.get('nome')
        if not(nome):return(self.encode({'codigo':'','message':'Campo obrigatorio não informado'}))
        if codigo=='novo':
            self.db.execute('INSERT INTO agenda(nome) VALUES(?)',(nome,))
            codigo = self.db.execute("SELECT last_insert_rowid() FROM agenda").fetchone()[0]
            self.db.commit()
            return(self.encode({'codigo':codigo,'message':'Registro inserido com sucesso'}))
        else:
            self.db.execute("UPDATE agenda SET nome=? WHERE codigo=?",(nome,codigo))
            self.db.commit()
            return(self.encode({'codigo':codigo,'message':'Registro alterado com sucesso'}))

if __name__=="__main__":
    exec(compile(open('main.py', "rb").read(), 'main.py', 'exec'),{__name__:"__main__"})

