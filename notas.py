#
#-*-coding:utf-8-*-
#
from Bottle import request,response,abort
import json

class Notas:
    def __init__(self,server):
        self.db=server.db;
        self.h=server.helpers
        server.route('/notas/salvar',['POST'],self.salvar)
        server.route('/notas/lista',['POST','GET'],self.lista)
        server.route('/notas/abrir',['POST','GET'],self.abrir)
        server.route('/notas/apagar',['POST','GET'],self.apagar)

    def salvar(self):
        id=request.forms.get('id')
        descricao=request.forms.get('descricao')
        valor=request.forms.get('valor')
        if not(id) or not(descricao) or not(valor):return("Campo requerido não informado")
        if id.lower()!='novo':
            self.db.execute("UPDATE notas SET valor=?,descricao=? WHERE id=?",(valor,descricao,id))
            self.db.commit()
            return("Registro editado com sucesso")
        else:
            contador=self.db.execute("SELECT count(descricao) FROM notas WHERE descricao=?",(descricao,)).fetchone()[0]
            if contador<=0:
                self.db.execute("INSERT INTO notas(descricao,valor) VALUES(?,?)",(descricao,valor))
                self.db.commit()
                return("Registro inserido")
            else:return("Registro já existente!")

    def abrir(self):
        id=request.params.get('id')
        if not(id):abort(401,'Campo obrigatorio não informado')
        campo=self.db.execute('SELECT id,descricao,valor FROM notas WHERE id=?',(id,)).fetchone()
        return (json.encoder.JSONEncoder().encode({"id":campo[0],"descricao":campo[1],"valor":campo[2]}))

    def lista(self):
        saida="<table>"
        for campo in self.db.execute("SELECT id,descricao FROM notas ORDER BY descricao").fetchall():
            saida+="<tr>"
            saida+="<td><a href='javascript:notas.abrir(\""+str(campo[0])+"\");'>"+campo[1]+"</a></td>"
            saida+='<td><img src="/static/imagens/lixeira.png" onClick="javascript:notas.apagar(\'%i\');"></td>'%campo[0]
            saida+="</tr>"
        saida+="</table>"
        return(saida)

    def apagar(self):
        id=request.params.get('id')
        if not(id):abort(401,'Campo obrigatorio não informado')
        self.db.execute("DELETE FROM notas WHERE id=?",(id,))
        self.db.commit()
        return("Registro apagado")
        

        
