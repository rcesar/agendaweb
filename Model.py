#
#-*-coding:utf-8-*-

import sqlite3.dbapi2 as sqlite
import os

sql = {
    'compromissos':'CREATE TABLE compromissos(id integer primary key autoincrement,data date,descricao char(50),valor text)',
    'pendencias':'CREATE TABLE pendencias(id integer primary key autoincrement,descricao char(50),valor text)',
    'notas':'CREATE TABLE notas(id integer primary key autoincrement,descricao char(50),valor text)',
    'agenda':'CREATE TABLE agenda(codigo integer primary key autoincrement,nome char(200))',
    'telefones':'CREATE TABLE telefones(codigo integer,descricao char(100),valor char(30))',
    'internet':'CREATE TABLE internet(codigo integer,descricao char(100),valor char(200))',
    'enderecos':'CREATE TABLE enderecos(codigo integer,descricao char(100),rua char(150),bairro char(50),cidade char(50),estado char(2),cep char(10))'
}
class Model:
    def __init__(self,server):
        self.db=sqlite.connect(os.path.join(server.appdir,'db','agenda.db'))
        self.db.text_factory=str
        self.execute=self.db.execute
        self.commit=self.db.commit
        for nome in list(sql.keys()):
            contador=int(self.db.execute("SELECT count(name) FROM SQLITE_MASTER WHERE name=?",(nome,)).fetchone()[0])
            if contador<=0:
                print(("Criando tabela "+nome))
                self.db.execute(sql[nome])
        
