#
#-*-coding:utf-8-*-
#
from Bottle import request,response,abort
import json




class Compromissos:
    def __init__(self,server):
        self.db=server.db;
        self.h=server.helpers
        server.route('/compromissos/salvar',['POST'],self.salvar)
        server.route('/compromissos/lista',['POST','GET'],self.lista)
        server.route('/compromissos/abrir',['POST','GET'],self.abrir)
        server.route('/compromissos/apagar',['POST','GET'],self.apagar)

    def salvar(self):
        try:
            id=request.forms.get('id')
            data=self.h.data2db(request.forms.get('data'))
            descricao=request.forms.get('descricao')
            valor=request.forms.get('valor')
        except Exception as e:
            return(str(e))        
        if id.lower()!='novo':
            self.db.execute("UPDATE compromissos SET valor=?,data=?,descricao=? WHERE id=?",(valor,data,descricao,id))
            self.db.commit()
            return("Registro editado com sucesso")
        else:
            contador=self.db.execute("SELECT count(descricao) FROM compromissos WHERE descricao=? AND data=?",(descricao,data)).fetchone()[0]
            if contador<=0:
                self.db.execute("INSERT INTO compromissos(data,descricao,valor) VALUES(?,?,?)",(data,descricao,valor))
                self.db.commit()
                return("Registro inserido")
            else:return("Registro já existente!")

    def abrir(self):
        id=request.params.get('id')
        if not(id):abort(401,'Campo obrigatorio não informado')
        campo=self.db.execute('SELECT id,data,descricao,valor FROM compromissos WHERE id=?',(id,)).fetchone()
        return (json.encoder.JSONEncoder().encode({"id":campo[0],"data":self.h.db2data(campo[1]),"descricao":campo[2],"valor":campo[3]}))

    def lista(self):
        try:
            data=request.params.get('data')
        except Exception as e:
            return(str(e))
        if not(data):return('Nenhuma data informada')
        saida="<table>"
        for campo in self.db.execute("SELECT id,data,descricao FROM compromissos WHERE data<=? ORDER BY data,descricao",(data,)).fetchall():
            saida+="<tr>"
            saida+="<td><a href='javascript:compromissos.abrir(\""+str(campo[0])+"\");'>"+self.h.db2data(campo[1])+"</a></td>"
            saida+="<td><a href='javascript:compromissos.abrir(\""+str(campo[0])+"\");'>"+campo[2]+"</a></td>"
            saida+='<td><img src="/static/imagens/lixeira.png" onClick="javascript:compromissos.apagar(\'%i\');"></td>'%campo[0]
            saida+="</tr>"
        saida+="</table>"
        return(saida)

    def apagar(self):
        id=request.params.get('id')
        if not(id):abort(401,'Campo obrigatorio não informado')
        self.db.execute("DELETE FROM compromissos WHERE id=?",(id,))
        self.db.commit()
        return("Registro apagado")
        

        
