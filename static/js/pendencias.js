//
//
var pendencias = {
    nova:function()
    {
        _('pendencia_id').value='novo';
        _('pendencia_descricao').value='';
        _('pendencia_valor').value='';
        dlg = $('#dlgpendencia').dialog({
        modal:true,width:400,height:400,title:"Pendencia",
        buttons:[{
            text:"Salvar",
            click:function(){pendencias.salvar(this);$( this ).dialog( "close" );}},
            {
            text:"Cancelar",
            click:function(){$( this ).dialog( "close" );}
             }]
         });   
    },
    abrir:function(id)
    {   
        req=$.ajax({url:'/pendencias/abrir',data:{id:id}});
        req.done(function(ret)
        {
            dlg = $('#dlgpendencia').dialog({
            modal:true,width:400,height:400,title:"Compromisso",
                buttons:[{
                    text:"Salvar",
                    click:function(){pendencias.salvar(this);$( this ).dialog( "close" );}},
                    {
                    text:"Cancelar",
                    click:function(){$( this ).dialog( "close" );}}]});           
            valores=JSON.parse(ret);
            _('pendencia_id').value=valores['id'];
            _('pendencia_descricao').value=valores['descricao'];
            _('pendencia_valor').value=valores['valor'];
        });
      req.fail(function(ajx,message){alert(' '+message+'\n '+ajx.status+':'+ajx.statusText);});
    },
    apagar:function(id)
    {   
        if (confirm('Apagar pendencia?'))
        {
            sender=$.ajax({
                url:'/pendencias/apagar',
                data:{id:id},
                method:'POST'
            });
            sender.done(function(ret){pendencias.atualizar();alert(ret);});
            sender.fail(function(ajx,message){alert(' '+message+'\n '+ajx.status+':'+ajx.statusText);});
        }
    },
    salvar:function(dlg)
    {
        id=_('pendencia_id').value;
        descricao=_('pendencia_descricao').value;
        valor=_('pendencia_valor').value;
        sender=$.ajax({
            data:{id:id,descricao:descricao,valor:valor},
            url:'/pendencias/salvar',
            method:'POST',
        });
        sender.done(function(ret)
        {
            alert(ret);
            pendencias.atualizar()
        });
        sender.fail(function(ajx,message)
        {
            alert(' '+message+'\n '+ajx.status+':'+ajx.statusText);
            });
    },
  atualizar:function()
    {
        data=document.getElementById("data").value.split("/");
        data=data[2]+'-'+data[1]+'-'+data[0];
        req = $.ajax({url:'/pendencias/lista'});
        req.done(function(ret){document.getElementById("pendencias").innerHTML=ret;});
        req.fail(function(ajx,message){alert(' '+message+'\n '+ajx.status+':'+ajx.statusText);});
    }  
};
