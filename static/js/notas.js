//
//
var notas = {
    nova:function()
    {
        _('nota_id').value='novo';
        _('nota_descricao').value='';
        _('nota_valor').value='';
        dlg = $('#dlgnota').dialog({
        modal:true,width:600,height:400,title:"nota",
        buttons:[{
            text:"Salvar",
            click:function(){notas.salvar(this);$( this ).dialog( "close" );}},
            {
            text:"Cancelar",
            click:function(){$( this ).dialog( "close" );}
             }]
         });   
    },
    abrir:function(id)
    {   
        req=$.ajax({url:'/notas/abrir',data:{id:id}});
        req.done(function(ret)
        {
            dlg = $('#dlgnota').dialog({
            modal:true,width:600,height:400,title:"Compromisso",
                buttons:[{
                    text:"Salvar",
                    click:function(){notas.salvar(this);$( this ).dialog( "close" );}},
                    {
                    text:"Cancelar",
                    click:function(){$( this ).dialog( "close" );}}]});           
            valores=JSON.parse(ret);
            _('nota_id').value=valores['id'];
            _('nota_descricao').value=valores['descricao'];
            _('nota_valor').value=valores['valor'];
        });
      req.fail(function(ajx,message){alert(' '+message+'\n '+ajx.status+':'+ajx.statusText);});
    },
    apagar:function(id)
    {   
        if (confirm('Apagar nota?'))
        {
            sender=$.ajax({
                url:'/notas/apagar',
                data:{id:id},
                method:'POST'
            });
            sender.done(function(ret){notas.atualizar();alert(ret);});
            sender.fail(function(ajx,message){alert(' '+message+'\n '+ajx.status+':'+ajx.statusText);});
        }
    },
    salvar:function(dlg)
    {
        id=_('nota_id').value;
        descricao=_('nota_descricao').value;
        valor=_('nota_valor').value;
        sender=$.ajax({
            data:{id:id,descricao:descricao,valor:valor},
            url:'/notas/salvar',
            method:'POST',
        });
        sender.done(function(ret)
        {
            alert(ret);
            notas.atualizar()
        });
        sender.fail(function(ajx,message)
        {
            alert(' '+message+'\n '+ajx.status+':'+ajx.statusText);
            });
    },
  atualizar:function()
    {
        data=document.getElementById("data").value.split("/");
        data=data[2]+'-'+data[1]+'-'+data[0];
        req = $.ajax({url:'/notas/lista'});
        req.done(function(ret){document.getElementById("notas").innerHTML=ret;});
        req.fail(function(ajx,message){alert(' '+message+'\n '+ajx.status+':'+ajx.statusText);});
    },

  criptografia:function()
    {
      _('senha').value='';
      dlg = $('#dlgsenha').dialog({
            modal:true,width:300,height:250,title:"Insira a senha",
                buttons:[{
                    text:"Ok",
                    click:function(){
                        try{
                    		chave=document.getElementById('senha').value;
                    		message=sjcl.encrypt(chave,document.getElementById("nota_valor").value);
                    		document.getElementById('nota_valor').value=encode(message);
                        	} catch(e) {alert(e);}
                        $(this).dialog( "close" );
                        }},
                    {
                    text:"Cancelar",
                    click:function(){$(this).dialog( "close" );}}]});           
    },
  decriptografia:function()
    {
      _('senha').value='';
      dlg = $('#dlgsenha').dialog({
            modal:true,width:300,height:250,title:"Insira a senha",
                buttons:[{
                    text:"Ok",
                    click:function(){
                	try{
		                chave=document.getElementById('senha').value;
                		message=Base64.decode(document.getElementById("nota_valor").value);
                		document.getElementById('nota_valor').value=sjcl.decrypt(chave,message);;
                    	} catch(e) {alert(e);}
                        $(this).dialog( "close" );
                        }},
                    {
                    text:"Cancelar",
                    click:function(){$(this).dialog( "close" );}}]});           
    }

};
