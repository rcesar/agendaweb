/*

*/
function _(pad) { return(document.getElementById(pad));}
//===========================
function abrir(codigo)
{
 ret=$.ajax({url:'/cadastro/abrir/'+codigo});
 ret.done(function(ret) {
        _('codigo').value=codigo;
        _('nome').value=ret;
        atualizar(codigo,'telefones');
        atualizar(codigo,'enderecos');
        atualizar(codigo,'internet');        
        });
 ret.fail(function(ajx,mess){alert(mess+'\n'+ajx.status+':'+ajx.statusText)})
}    
function busca()
{
 padrao="%"+_("pad").value+"%";
 ret=$.ajax({
        url:'/cadastro/buscar',
        method:'post',
        data:{pad:padrao}
        });
 ret.done(function(ret) {_('resultado').innerHTML=ret;});
 ret.fail(function(ajx,mess){alert(mess+'\n'+ajx.status+':'+ajx.statusText)})
}
function salvar()
{
  codigo=_('codigo').value;
  nom=_('nome').value;
  if (nome=='')
    {
        alert('Nome esta em branco');
        return;
    }
  if (codigo=='') { codigo='novo';}   
  req=$.ajax({ url:'/cadastro/salvar/'+codigo,data:{nome:nom},method:'post'});
  req.done(function(ret) {
         ret = JSON.parse(ret)
         _('codigo').value=ret['codigo'];
         alert(ret['message']);
        atualizar(_('codigo').value,'telefones');
        atualizar(_('codigo').value,'enderecos');
        atualizar(_('codigo').value,'internet');        
        })
  req.fail(function(ajx,mess){alert(mess+'\n'+ajx.status+':'+ajx.statusText)})
}
function novo()
{
 _('codigo').value='';
 _('nome').value='';
 _('telefones').innerHTML='';
 _('enderecos').innerHTML='';
 _('internet').innerHTML='';
}
function atualizar(codigo,tipo)
{
 url = '/cadastro/listar/'+tipo+'/'+codigo;
 ret=$.ajax({url:url})
 ret.done(function(ret){_(tipo).innerHTML=ret;});
 ret.fail(function(ajx,mess){alert(mess+'\n'+ajx.status+':'+ajx.statusText)})
}
function apagar(codigo)
{
       if (confirm("Apagar os dados?\nTodas as informações serão perdidas."))
        {
            req=$.ajax({url:'/cadastro/apagar/'+codigo});
            req.done(function(ret){alert(ret);novo();});
            req.fail(function(ajx,mess){alert(mess+'\n'+ajx.status+':'+ajx.statusText);$(dlg).dialog('close');});       
        } 
}
//===============================================================================
var internet = {
 novo:function()
    {
      _('telefone_rowid').value='novo';
      _('telefone_descricao').value='';
      _('telefone_valor').value='';
      $("#dlgtelefone").dialog({title:"Novo endereço web",modal:true,
        buttons:[{text:'Salvar',click:function(){internet.salvar(this);}},{text:'Cancelar',click:function(){$(this).dialog('close');}}]
        });
    },
 salvar:function(dlg)
    {
        codigo=_('codigo').value;
        rowid=_('telefone_rowid').value;
        descricao=_('telefone_descricao').value;
        valor=_('telefone_valor').value;
        data = {rowid:rowid,descricao:descricao,valor:valor}
        req=$.ajax({url:'/cadastro/internet/salvar/'+codigo,data:data,method:'post'});
       req.done(function(ret){alert(ret); atualizar(codigo,'internet');$(dlg).dialog('close');});
       req.fail(function(ajx,mess){alert(mess+'\n'+ajx.status+':'+ajx.statusText);$(dlg).dialog('close');});       
    },
 apagar:function(rowid)
    {
        if (confirm("Apagar o endereço?"))
        {
            req=$.ajax({url:'/cadastro/internet/apagar/'+rowid});
            req.done(function(ret){alert(ret);atualizar(_('codigo').value,'internet');});
            req.fail(function(ajx,mess){alert(mess+'\n'+ajx.status+':'+ajx.statusText);$(dlg).dialog('close');});       
        }
    },
 abrir:function(rowid)
    {
      req=$.ajax({url:'/cadastro/internet/abrir/'+rowid});
      req.done(function(ret)
        {
           ret=JSON.parse(ret);
          _('telefone_rowid').value=ret.rowid;
          _('telefone_descricao').value=ret.descricao;
          _('telefone_valor').value=ret.valor;
          $("#dlgtelefone").dialog({title:"Editando endereço web:"+ret.descricao,modal:true,
            buttons:[{text:'Salvar',click:function(){internet.salvar(this);}},{text:'Cancelar',click:function(){$(this).dialog('close');}}]});
        });
      req.fail(function(ajx,mess){alert(mess+'\n'+ajx.status+':'+ajx.statusText);$(dlg).dialog('close');});
     },

}
//==========================================
var telefones = {
 novo:function()
    {
      _('telefone_rowid').value='novo';
      _('telefone_descricao').value='';
      _('telefone_valor').value='';
      $("#dlgtelefone").dialog({title:"Novo telefone",modal:true,
        buttons:[{text:'Salvar',click:function(){telefones.salvar(this);}},{text:'Cancelar',click:function(){$(this).dialog('close');}}]
        });
    },
 salvar:function(dlg)
    {
        codigo=_('codigo').value;
        rowid=_('telefone_rowid').value;
        descricao=_('telefone_descricao').value;
        valor=_('telefone_valor').value;
        data = {rowid:rowid,descricao:descricao,valor:valor}
        req=$.ajax({url:'/cadastro/telefone/salvar/'+codigo,data:data,method:'post'});
       req.done(function(ret){alert(ret); atualizar(codigo,'telefones');$(dlg).dialog('close');});
       req.fail(function(ajx,mess){alert(mess+'\n'+ajx.status+':'+ajx.statusText);$(dlg).dialog('close');});       
    },
 apagar:function(rowid)
    {
        if (confirm("Apagar o telefone?"))
        {
            req=$.ajax({url:'/cadastro/telefone/apagar/'+rowid});
            req.done(function(ret){alert(ret);atualizar(_('codigo').value,'telefones');});
            req.fail(function(ajx,mess){alert(mess+'\n'+ajx.status+':'+ajx.statusText);$(dlg).dialog('close');});       
        }
    },
 abrir:function(rowid)
    {
      req=$.ajax({url:'/cadastro/telefone/abrir/'+rowid});
      req.done(function(ret)
        {
           ret=JSON.parse(ret);
          _('telefone_rowid').value=ret.rowid;
          _('telefone_descricao').value=ret.descricao;
          _('telefone_valor').value=ret.valor;
          $("#dlgtelefone").dialog({title:"Editando telefone:"+ret.descricao,modal:true,
            buttons:[{text:'Salvar',click:function(){telefones.salvar(this);}},{text:'Cancelar',click:function(){$(this).dialog('close');}}]});
        });
      req.fail(function(ajx,mess){alert(mess+'\n'+ajx.status+':'+ajx.statusText);$(dlg).dialog('close');});
     },

}
//==========================================
var enderecos = {
 abrir:function(rowid)
    {
      req=$.ajax({url:'/cadastro/endereco/abrir/'+rowid});
      req.done(function(ret)
        {
           ret=JSON.parse(ret);
          _('endereco_rowid').value=ret.rowid;
          _('endereco_descricao').value=ret.descricao;
          _('endereco_rua').value=ret.rua;
          _('endereco_bairro').value=ret.bairro;
          _('endereco_cidade').value=ret.cidade;
          _('endereco_estado').value=ret.estado;
          _('endereco_cep').value=ret.cep;            
          $("#dlgendereco").dialog({title:"Editando endereço:"+ret.descricao,modal:true,
            buttons:[{text:'Salvar',click:function(){enderecos.salvar(this);}},{text:'Cancelar',click:function(){$(this).dialog('close');}}]});
        });
      req.fail(function(ajx,mess){alert(mess+'\n'+ajx.status+':'+ajx.statusText);$(dlg).dialog('close');});
     },
 novo:function()
    {
      _('endereco_rowid').value='novo';
      _('endereco_descricao').value='';
      _('endereco_rua').value='';
      _('endereco_bairro').value='';
      _('endereco_cidade').value='';
      _('endereco_estado').value='';
      _('endereco_cep').value='';            
      $("#dlgendereco").dialog({title:"Novo endereço",modal:true,
        buttons:[{text:'Salvar',click:function(){enderecos.salvar(this);}},{text:'Cancelar',click:function(){$(this).dialog('close');}}]
        });
    },
 salvar:function(dlg)
    {
     if (_('codigo')=='')
      {
         alert('Código não preenchido');
         return;
      }
       codigo=_('codigo').value;
       rowid=_('endereco_rowid').value;
       descricao=_('endereco_descricao').value;
       rua=_('endereco_rua').value;
       bairro=_('endereco_bairro').value;
       cidade=_('endereco_cidade').value;
       estado=_('endereco_estado').value;
       cep=_('endereco_cep').value;
       req=$.ajax({url:'/cadastro/endereco/salvar/'+codigo,
                  data:{rowid:rowid,descricao:descricao,rua:rua,bairro:bairro,cidade:cidade,estado:estado,cep:cep},method:'post'});
       req.done(function(ret){alert(ret); atualizar(codigo,'enderecos');$(dlg).dialog('close');});
       req.fail(function(ajx,mess){alert(mess+'\n'+ajx.status+':'+ajx.statusText);$(dlg).dialog('close');});       
    },
 apagar:function(rowid)
    {
        if (confirm("Apagar o endereço?"))
        {
            req=$.ajax({url:'/cadastro/endereco/apagar/'+rowid});
            req.done(function(ret){alert(ret);atualizar(_('codigo').value,'enderecos');});
            req.fail(function(ajx,mess){alert(mess+'\n'+ajx.status+':'+ajx.statusText);$(dlg).dialog('close');});       
        }
    }
}
