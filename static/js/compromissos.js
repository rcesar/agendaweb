//js dir()
//Object.keys($(dlg));
var compromissos = {
    novo:function()
    {
        //html ="<div id='dlg_novo_compromisso'>"+document.getElementById("dlgcompromisso").innerHTML+"</div>";
        _("compromisso_id").value="";
        _("compromisso_descricao").value="";
        _("compromisso_valor").value="";
        _("compromisso_data").value=document.getElementById("data").value;
        dlg = $('#dlgcompromisso').dialog({
        modal:true,width:400,height:400,title:"Compromisso",
        buttons:[{
            text:"Salvar",
            click:function(){compromissos.salvar(this);$( this ).dialog( "close" );}},
            {
            text:"Cancelar",
            click:function(){$( this ).dialog( "close" );}
             }]
         });   
         document.getElementById('compromisso_data').value=document.getElementById("data").value
         document.getElementById("compromisso_id").value="novo";
    },
    abrir:function(id)
    {   
        req=$.ajax({url:'/compromissos/abrir',data:{id:id}});
        req.done(function(ret)
        {
            var _ = function(id){return document.getElementById(id);}            
            valores=JSON.parse(ret);
            _('compromisso_id').value=valores['id'];
            _('compromisso_descricao').value=valores['descricao'];
            _('compromisso_data').value=valores['data'];
            _('compromisso_valor').value=valores['valor'];
            dlg = $('#dlgcompromisso').dialog({
            modal:true,width:400,height:400,title:"Compromisso",method:'POST',
                buttons:[{
                    text:"Salvar",
                    click:function(){compromissos.salvar(this);$( this ).dialog( "close" );}},
                    {
                    text:"Cancelar",
                    click:function(){$( this ).dialog( "close" );}}]});           
        });
      req.fail(function(ajx,message){alert(' '+message+'\n '+ajx.status+':'+ajx.statusText);});
    },
    apagar:function(id)
    {   
        if (confirm("Apagar o registro?\n"))
        {
            sender=$.ajax({
                url:'/compromissos/apagar',
                data:{id:id},
                method:'POST'
            });
            sender.done(function(ret){compromissos.atualizar();alert(ret);});
            sender.fail(function(ajx,message){alert(' '+message+'\n '+ajx.status+':'+ajx.statusText);});
        }
    },
    salvar:function(dlg)
    {
//        for(x=0;x<=dlg.children.length;x++){alert(x+'\n'+dlg.children[x]);}
//        return
        id=dlg.children[0].value;
        data = dlg.children[6].value;
        descricao=dlg.children[3].value;
        valor = dlg.children[8].value;
        dlg.children[0].value='';
        dlg.children[6].value='';
        dlg.children[3].value='';
        dlg.children[8].value='';
        sender=$.ajax({
            data:{id:id,data:data,descricao:descricao,valor:valor},
            url:'/compromissos/salvar',
            method:'POST',
        });
        sender.done(function(ret)
        {
            alert(ret);
            compromissos.atualizar();
        });
        sender.fail(function(ajx,message)
        {
            alert(' '+message+'\n '+ajx.status+':'+ajx.statusText);
            });
    },
  atualizar:function()
    {
        data=document.getElementById("data").value.split("/");
        data=data[2]+'-'+data[1]+'-'+data[0];
        req = $.ajax({url:'/compromissos/lista',data:{data:data}});
        req.done(function(ret){document.getElementById("compromissos").innerHTML=ret;});
        req.fail(function(ajx,message){alert(' '+message+'\n '+ajx.status+':'+ajx.statusText);});
    }  
};
